When(/^I go to the products page$/) do
  visit products_path
end

When(/^I click on Create Product$/) do
  within '.product' do
      click_link 'Create Product'
  end
end

Then(/^I should be redirected to new product page$/) do
  expect(page).to have_content('Enter Product Details')
end

Then(/^I should see a heading "([^"]*)"$/) do |message|
  expect(page).to have_content(message)
end

When(/^I enter the name as "([^"]*)"$/) do |name|
  within '.product' do
    fill_in 'name', with: name
  end
end

When(/^I enter the version as "([^"]*)"$/) do |version|
  within '.product' do
    fill_in 'version', with: version
  end
end

When(/^I click on Save$/) do
  within '.product' do
    click_button 'Save'
  end
end

Then(/^it should be redirected to the products page$/) do
  expect(page).to have_content('Products Listing')
end

Then(/^I should see a message "([^"]*)"$/) do |message|
  within '.flash' do
    expect(page).to have_content('Product created successfully')
  end
end

Then(/^I should see the products table with "([^"]*)"$/) do |product_name|
  expect(page).to have_content(product_name)
end

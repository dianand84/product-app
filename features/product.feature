Feature: Product
  Scenario: Go to Products page
    When I go to the products page
    Then I should see a heading "Products Listing"

  Scenario: Go to Create Product
    When I go to the products page
      And I click on Create Product
    Then I should be redirected to new product page

  Scenario: Create Product
    When I go to the products page
      And I click on Create Product
      And I enter the name as "Ion Standard"
      And I enter the version as "1.0"
      And I click on Save
    Then it should be redirected to the products page
      And I should see a message "Product created successfully"
      And I should see the products table with "Ion Standard"

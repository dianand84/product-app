class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :name, length: 50
      t.string :version, length: 5
    end
  end
end

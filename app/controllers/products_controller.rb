class ProductsController < ApplicationController
  def index
    @products = Product.all
  end

  def create
    Product.create!(name: product_params[:name], version: product_params[:version])
    flash[:notice] = 'Product created successfully'
    redirect_to products_path
  end

  private
  def product_params
    params.permit(:name, :version)
  end
end
